/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit4TestClass.java to edit this template
 */
package password;
import static junit.framework.Assert.assertEquals;

import org.junit.Test;


/**
 *
 * @author jayvy
 */

public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
  
    public static void setUpClass() {
    }
    

    public static void tearDownClass() {
    }
    
  
    public void setUp() {
    }
    
  
    public void tearDown() {
    }

    @Test
    public void testIsvalidPasswordcheckDigitRegular() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "abc0";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckDigitException() {
        System.out.println("isvalidPasswordException");
        String userInput = "abc";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckDigitBoundaryIn() {
        System.out.println("isvalidPasswordBoundaryIn");
        String userInput = "abc07";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckDigitBoundaryOut() {
        System.out.println("isvalidPasswordDigitBoundaryOut");
        String userInput = "abcddAk&ss";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
 @Test
    public void testIsvalidPasswordcheckCharRegular() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "a";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
   @Test
    public void testIsvalidPasswordcheckCharException() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckCharBoundaryIn() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "a";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckCharBoundaryOut() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckMinLengthRegular() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "abcddAk&";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckMinLengthException() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "abc0ddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckMinLengthBoundaryIn() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "abcfsddAk&";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
     @Test
    public void testIsvalidPasswordcheckMinLengtBoundaryOut() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "ac0ddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
      @Test
    public void testIsvalidPasswordcheckCapitalsRegular() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "ac0ddAk&";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
      @Test
    public void testIsvalidPasswordcheckCapitalsException() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "ac0ddk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
      @Test
    public void testIsvalidPasswordcheckCapitalsBoundaryIn () {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "ac0dDdAk&";
        boolean expResult = true;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
      @Test
    public void testIsvalidPasswordMinLengtBoundaryOut() {
        System.out.println("isvalidPasswordDigitRegular");
        String userInput = "ac0ddAk&";
        boolean expResult = false;
        boolean result = PasswordValidator.isvalidPassword(userInput);
        assertEquals(expResult, result);
      
    }
}
